package uz.pdp.ecommersapp.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.ecommersapp.entity.Category;
import uz.pdp.ecommersapp.entity.Category;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
    private Integer id;
    private  String name;
    private Double price;
    private Integer views;
    private Category category;
}
