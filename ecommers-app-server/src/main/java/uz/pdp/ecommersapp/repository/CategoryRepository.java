package uz.pdp.ecommersapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.ecommersapp.entity.Category;
import uz.pdp.ecommersapp.entity.Category;


@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
}
