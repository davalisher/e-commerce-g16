package uz.pdp.ecommersapp.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.ecommersapp.entity.Detail;
import uz.pdp.ecommersapp.entity.DetailType;

import java.util.List;

@Projection(name = "DetailProjection", types = {Detail.class})
public interface DetailProjection {
    Integer getId();

    String getName();

    String getValue();

    @Value("#{target.detailType!=null?target.detailType.id:null}")
    Integer getDetailTypeId();

    @Value("#{target.detailType!=null?target.detailType.name:null}")
    String getDetailTypeName();

    boolean isMain();
}
