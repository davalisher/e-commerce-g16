package uz.pdp.ecommersapp.service;

import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.ecommersapp.entity.Attachment;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

public interface AttachmentService {
    List<Attachment> save(MultipartHttpServletRequest request);
    void getFile(HttpServletResponse response, UUID id);
    void  deleteFile(UUID id);
}
