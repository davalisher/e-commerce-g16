package uz.pdp.ecommersapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.ecommersapp.entity.Category;
import uz.pdp.ecommersapp.payload.CategoryReq;
import uz.pdp.ecommersapp.payload.Result;
import uz.pdp.ecommersapp.repository.AttachmentRepository;
import uz.pdp.ecommersapp.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/category")
public class CategoriaController {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    @GetMapping("/take")
    public ResponseEntity<List<Category>> takeCategories() {
        List<Category> categories = categoryRepository.findAll();
        if (categories != null) {
            return ResponseEntity.ok(categories);
        } else {
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping("/save")
    public ResponseEntity<Result> saveCategory(@RequestBody CategoryReq categoryReq) {
        Category category = new Category();
        category.setName(categoryReq.getName());
        if (categoryReq.getParentId() == null) {
            category.setParentId(0);
        } else {
            category.setParentId(categoryReq.getParentId());
        }
        if (categoryReq.getIconId() != null) {
            category.setIcon(attachmentRepository.getOne(categoryReq.getIconId()));
        }
        Category savedCategory = categoryRepository.save(category);
        if (savedCategory != null) {
            Result result = new Result();
            result.setSuccess(true);
            result.setMessage(categoryReq.getName() + " successfully saved");
            return ResponseEntity.ok(result);
        } else {
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(categoryReq.getName() + " not saved.Server Error");
            return ResponseEntity.status(500).body(result);
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Result> editCategory(@PathVariable Integer id, @RequestBody CategoryReq categoryReq) {
        Optional<Category> optional = categoryRepository.findById(id);
        if (optional.isPresent()) {
            Category category = optional.get();
            category.setName(categoryReq.getName());
            if (categoryReq.getParentId() == null) {
                category.setParentId(0);
            } else {
                category.setParentId(categoryReq.getParentId());
            }
            if (categoryReq.getIconId() != null) {
                category.setIcon(attachmentRepository.getOne(categoryReq.getIconId()));
            }
            Category savedCategory = categoryRepository.save(category);
            if (savedCategory != null) {
                Result result = new Result();
                result.setSuccess(true);
                result.setMessage(categoryReq.getName() + "successfully saved");
                return ResponseEntity.ok(result);
            } else {
                Result result = new Result();
                result.setSuccess(false);
                result.setMessage(categoryReq.getName() + "not saved.Server Error");
                return ResponseEntity.status(500).body(result);
            }

        } else {
            Result result = new Result();
            result.setSuccess(false);
            result.setMessage(categoryReq.getName() + "not saved.Id not sended in response");
            return ResponseEntity.status(400).body(result);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Result> deleteCategory(@PathVariable Integer id) {
      try{  categoryRepository.deleteById(id);}
      catch (Exception e){
          e.printStackTrace();
      }

      try{ Category category = categoryRepository.getOne(id);
          Result result = new Result();
          result.setSuccess(false);
          result.setMessage(category.getName() + "not deleted.Server Error");
          return ResponseEntity.status(500).body(result);


      }

        catch (Exception e) {
            Result result = new Result();
            result.setSuccess(true);
            result.setMessage("successfully deleted");
            return ResponseEntity.ok(result);

        }

    }

}
