package uz.pdp.ecommersapp.controller;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.ecommersapp.entity.Attachment;
import uz.pdp.ecommersapp.service.AttachmentService;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/api/attachment")
public class AttachmentController {
    @Autowired
    AttachmentService attachmentService;

    @PostMapping("/upload")
    @ResponseBody
    public List<UUID> save(MultipartHttpServletRequest request){
       List< Attachment> attachments=attachmentService.save(request);
       List<UUID> uuids=new ArrayList<>();
       attachments.forEach(attachment -> {
          uuids.add( attachment.getId());
       });
        return uuids ;
    }
    @GetMapping("/getFile/{id}")
    public void getFile(HttpServletResponse response, @PathVariable UUID id){
        attachmentService.getFile(response,id);
    }

}
