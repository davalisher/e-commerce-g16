import axios from 'axios'
const initialState={
    products: [{
        id: 0,
        name: 'iphone',
        price: '1000$',
        categoria: {
            id: 0,
            name: 'phone',
            key: 0,
            is_main: false
        },
        details: [{
            id: 0,
            name: 'system',
            value: 'ios',
            detailTypes: {
                id: 0,
                name: 'all'
            }
        }],
        attachment: {
            id: 0,
            size: '0',
            extern: 'null',
            name: 'pic',
            originalName: 'picNotAdded',
            attachmentContent: {
                id: '0',
                file: 'dadsadda'
            }
        }
    }],
    categories: [{name: 'phone'}, {name: 'tv'}, {name: 'computer'}],
    details: [],
    setProduct: false,
    drawDetails: [{
        id: 1,
        name: 'system',
        value: 'ios',
        detailTypes: {
            id: 1,
            name: 'all'
        }
    },
    ],
    detailTypes: [{
        id: 1,
        name: 'all'
    }],
    editProduct: {
        id: 0,
        name: ' ',
        price: ' ',
        categoria: {id: 0, name: ' ', key: 0, is_main: false},
        details: [{id: 0, name: ' ', value: ' ', detailTypes: {id: 0, name: ' '}}],
        attachment: {
            id: 0,
            size: ' ',
            extern: ' ',
            name: ' ',
            originalName: ' ',
            attachmentContent: {id: ' ', file: ' '}
        }
    }, form: null
};
const intializeStatef=()=>{
    initialState


    return initialState
}
export const productsAdmin=(state=null,action)=>{state=intializeStatef();
    switch (action.type) {
        case 'SET_PRODUCTS':
            return{...state,
                items:action.payload,
                isReady:true
            }
        case 'SET_ISREADY':
            return{...state,
                isReady:true
            }
        default: return state
    }
}
