import React from 'react';
import {CardGroup, Container, Row, Col} from 'react-bootstrap';
import Home from './components/home';
import Login from './components/login';
import SignUp from './components/SignUp';
import Admin from './components/admin/index';
import Client from './components/client/index';
import AuthAdmin from './components/authAdmin';
import {Switch, BrowserRouter, Route, withRouter} from 'react-router-dom';

class App extends React.Component {
    render() {
        return (
            <Container>
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/signup" component={SignUp}/>
                        <Route path="/admin" component={Admin}/>
                        <Route path="/client" component={Client}/>
                    </Switch>
                </BrowserRouter>
            </Container>
        );
    }

}

export default App;
