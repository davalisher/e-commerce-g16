/**
 * Created by Pinup on 02.07.2019.
 */
import React from 'react';
import {connect} from 'react-redux'
import axios from 'axios'
import {setUser} from '../actions/user'
import {Container, Row, Col, Button} from 'reactstrap';
import {AvForm, AvField} from 'availity-reactstrap-validation';
import './login.css'

class Login extends React.Component {
    componentDidMount() {
        this.getUser();
    }

    login = (e, values) => {

        axios({
            method: 'post',
            url: '/api/auth/signin',
            data: values
        }).then(res => {
            if (res.data.tokenType) {
                localStorage.setItem('ecommerce-token', res.data.tokenType + ' ' + res.data.accessToken);
                this.getUser();
            }
        })
    };

    getUser = () => {
        if (localStorage.getItem('ecommerce-token')) {
            const {setUsers} = this.props;
            axios({
                url: '/api/auth/user',
                headers: {Authorization: localStorage.getItem('ecommerce-token')}
            }).then(res => {
                if (res.data) {
                    setUsers(res.data);
                    const roles = res.data.roles;
                    if (roles.filter(r => r.name === 'ROLE_ADMIN').length > 0) {
                        this.props.history.push('/admin');
                    } else if (roles.filter(r => r.name === 'ROLE_USER').length > 0) {
                        this.props.history.push('/client');
                    }
                }
            })
        }
    };

    render() {
        return (
            <Container>
                <Row>
                    <Col className="loginFormBox col-md-4 offset-md-4">
                        <h4 className='singUpTitle'> Login </h4>
                        <AvForm onValidSubmit={this.login}>
                            <AvField name="username"
                                     type="text" errorMessage="Invalid username"
                                     label='Username'
                                     className='loginForm'
                                     validate={{required: {value: true}}}/>
                            <AvField name="password"
                                     type="password" errorMessage="Invalid password"
                                     label='Password'
                                     className='loginForm'
                                     validate={{required: {value: true}}}/>
                            <div className='text-center'>
                                <Button className='loginBtn mt-1 mb-3'> Login </Button>
                            </div>
                        </AvForm>
                    </Col>
                </Row>
            </Container>
        );
    }

}

// const mapStateToProps = state => ({...state})

const mapDispatchToProps = dispatch => ({
    setUsers: user => dispatch(setUser(user))
});

export default connect(null, mapDispatchToProps)(Login);
