import React from 'react';
import {Button, InputGroup, Navbar} from "react-bootstrap";
import Cart from './cart'
import '../components/menu.css'
import {FaBars, FaSearch} from "react-icons/fa";
import {Input, InputGroupAddon} from "reactstrap";
import {Link} from 'react-router-dom';

export default class Menu extends React.Component {
    constructor() {
        super();

        this.state = {

            showCabinetMenu: false, // cabinet ga tegishli

            backgroundColor: '#f5f5f5',
            boxShadow: '',
        };

        this.showCabinet = this.showCabinet.bind(this);  // cabinet ga tegishli
        this.closeCabinet = this.closeCabinet.bind(this);  // cabinet ga tegishli
    };

    // ******************************menu boxShadow ga tegishli********************************
    listenScrollEvent = e => {
        if (window.scrollY > 140) {
            this.setState({backgroundColor: '#ffffff'});
            // this.setState({backgroundColor: 'rgba(105,121,120,0.57)'});
            this.setState({boxShadow: '0 10px 15px 0 rgba(105,121,120,0.57'})
        } else {
            this.setState({backgroundColor: '#f5f5f5'});
            this.setState({boxShadow: ''})
        }
    };

    componentDidMount() {
        window.addEventListener('scroll', this.listenScrollEvent)
    }

    // ******************************menu boxShadow ga tegishli********************************

    // ******************************cabinet ga tegishli********************************
    showCabinet(event) {
        event.preventDefault();

        this.setState({showCabinetMenu: true}, () => {
            document.addEventListener('click', this.closeCabinet);
        });
    }

    closeCabinet(event) {

        // if (!this.dropdownCabinetMenu.contains(event.target)) {
        //
        this.setState({showCabinetMenu: false}, () => {
            document.removeEventListener('click', this.closeCabinet);
        });
        //
        // }
    }

    // ******************************cabinet ga tegishli********************************

    render() {
        const {cartProducts} = this.props;

        return (
            <Navbar sticky="top" expand="lg"
                    style={{backgroundColor: this.state.backgroundColor, boxShadow: this.state.boxShadow}}>
                <Navbar.Brand className='navbarBrand' href="#">eCommerce</Navbar.Brand>
                <Navbar.Toggle className='navbarToggle' aria-controls="basic-navbar-nav">
                    <FaBars/>
                </Navbar.Toggle>
                <Navbar.Collapse id="basic-navbar-nav">
                    <button type='button' className='navCategory'>KATEGORIYALAR</button>
                    <InputGroup className='navInputGroup'>
                        <Input type='text' className='navSearch' placeholder="Search"/>
                        <InputGroupAddon addonType="append">
                            <button type="submit" value="Submit" className='inputBtn'><FaSearch className='btnIcon'
                                                                                                style={{color: 'white'}}/>
                                Qidiruv
                            </button>
                        </InputGroupAddon>
                    </InputGroup>
                    <div>
                        <button type='button' className='navCabinet'
                                onClick={this.showCabinet}>KABINET
                        </button>
                        {
                            this.state.showCabinetMenu
                                ? (
                                    <ul className='cabinetBox' ref={(element) => {
                                        this.dropdownCabinetMenu = element;
                                    }}>
                                        <li>
                                            <Link to="/login" style={{textDecoration: 'none'}}>
                                                <div className='cabinetBtn1'>
                                                    KIRISH
                                                </div>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="/signup" style={{textDecoration: 'none'}}>
                                                <div className='cabinetBtn2'>
                                                    REGISTRATSIYADAN O`TISH
                                                </div>
                                            </Link>
                                        </li>
                                    </ul>
                                )
                                : (
                                    null
                                )
                        }
                    </div>
                </Navbar.Collapse>
                <Cart products={cartProducts}/>
            </Navbar>
        )
    }
}


