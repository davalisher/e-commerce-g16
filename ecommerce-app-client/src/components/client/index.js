/**
 * Created by Pinup on 04.07.2019.
 */
import React from 'react';
import {CardGroup, Container, Row, Col} from 'react-bootstrap';
import ClientHome from './clientHome';
import AuthClient from '../authClient';
import {Switch, BrowserRouter, Route, withRouter} from 'react-router-dom';

class Client extends React.Component {
    render() {
        return (
            <Container>
                <BrowserRouter>
                    <Switch>
                        <AuthClient>
                            <Route path="/client" component={ClientHome}/>
                        </AuthClient>
                    </Switch>
                </BrowserRouter>
            </Container>
        );
    }

}

export default Client;
