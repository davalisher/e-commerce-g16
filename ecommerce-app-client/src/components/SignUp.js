/**
 * Created by Pinup on 04.07.2019.
 */
import React, {Component} from 'react';
import axios from 'axios';
import {Container, Row, Col, Button} from 'reactstrap';
import {AvForm, AvField} from 'availity-reactstrap-validation';
import './signup.css'

class SignUp extends Component {
    signUp = (e, data) => {
        if (data.password === data.prePassword) {
            axios({
                url: '/api/auth/signup',
                method: 'post',
                data
            }).then(res => {
                alert(res.data);
                this.props.history.push('/login');
            }).catch(e => {
                alert('Xatolik');
            })
        } else {
            alert('parolni to\'g\'ri kiriting');
        }
    };

    render() {
        return (
            <Container>
                <Row>
                    <Col className="signUpFormBox col-md-4 offset-md-4">
                        <h4 className='singUpTitle'> Sign Up </h4>
                        <AvForm onValidSubmit={this.signUp}>
                            <AvField name="username"
                                     className='signUpForm'
                                     label="Username"
                                     type="text" errorMessage="Invalid username"
                                     validate={{required: {value: true}}}/>
                            <AvField name="email"
                                     className='signUpForm'
                                     label="Email"
                                     type="email" errorMessage="Invalid email"
                                     validate={{required: {value: true}}}/>
                            <AvField name="name"
                                     className='signUpForm'
                                     label="Name"
                                     type="text" errorMessage="Invalid name"
                                     validate={{required: {value: true}}}/>
                            <AvField name="password"
                                     className='signUpForm'
                                     label="Password"
                                     type="password" errorMessage="Invalid password"
                                     validate={{required: {value: true}}}/>
                            <AvField name="prePassword"
                                     className='signUpForm'
                                     label="PrePassword"
                                     type="password" errorMessage="Invalid prePassword"
                                     validate={{required: {value: true}}}/>
                            <div className='text-center'>
                                <Button className='signUpBtn mt-1 mb-3'> SignUp </Button>
                            </div>
                        </AvForm>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default SignUp;