import React from 'react';
import {connect} from 'react-redux';
import {Button, Card} from "react-bootstrap";
import {addToCart} from "../actions/cart";
import '../components/product.css'

class Product extends React.Component{
    render() {
        const {product,addToCart} = this.props;
        return (
            <div className='cardBox mt-4 mb-4' key={product.id}>
                <Card.Img className='cardImg' src={product.img}/>
                <Card.Body>
                    <Card.Title style={{ fontSize: '12px'}}>{product.name}</Card.Title>
                    <Card.Text style={{ fontSize: '12px'}}>
                        {product.price}
                    </Card.Text>
                    <Button className='cardBtn' onClick={addToCart.bind(this,product)}>Add to cart</Button>
                </Card.Body>
            </div>
        )
    }
}
// const mapStateToProps = state => ({...state});
const mapDispatchToProps=dispatch=>({
    addToCart: product=>dispatch(addToCart(product))
});
export default connect(null,mapDispatchToProps)(Product)