import React from 'react'
import {connect} from 'react-redux';
import {DropdownButton, Dropdown, Button} from 'react-bootstrap'

// import {deleteFromCart} from "../actions/cart";


class Register extends React.Component {
    logOut = () => {
        if (localStorage.getItem('ecommerce-token')) {
            localStorage.removeItem('ecommerce-token');
        }


    };

    render() {
        let user = this.props.user;
        let title;
        let unRegistered = true;
        if (user) {
            title = user.name;
            unRegistered = false;
        } else {
            title = "Register";
        }
        console.log(this.props.prods.user);
        console.log("user");
        let hasToken = !localStorage.getItem('ecommerce-token');

        console.log(hasToken + 'hasToken');
        return <DropdownButton id="dropdown-basic-button" title={title}>

            {unRegistered ? <Dropdown.Item>
                <Button>sign in</Button>
                <Button>sign up</Button>
                <Button variant="danger" onClick={this.logOut}>log out</Button>
            </Dropdown.Item> : ""}

        </DropdownButton>
    }
}

const mapStateToProps = state => ({...state});

// const mapDispatchToProps=dispatch=>({
//     deleteFromCart: id=>dispatch(deleteFromCart(id))
// })
export default connect(mapStateToProps)(Register)
// export default Register