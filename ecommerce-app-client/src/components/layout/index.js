/**
 * Created by Pinup on 04.07.2019.
 */
import React from 'react';
import {connect} from 'react-redux'
import {Button} from "reactstrap";
import Menu from '../menu'
import Footer from '../footer'
import OfferSection from '../offerSection'
import {CardGroup, Container, Row, Col} from 'react-bootstrap';

class Layout extends React.Component {

    render() {
        const cartProducts = this.props.cart.items;
        return (
            <div className="Sections">
                <Container classNmae="position-relative">
                    <Menu cartProducts={cartProducts}/>
                    {this.props.children}
                </Container>
                <OfferSection/>
                <Footer/>
            </div>
        );
    }

}

const mapStateToProps = state => ({...state});

export default connect(mapStateToProps)(Layout);