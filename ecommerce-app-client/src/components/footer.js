import React from 'react';
import {Button, Container, Form, Nav, Row} from "react-bootstrap";

export default class Footer extends React.Component {
    render() {
        return (
                <Container classNmae="position-relative">
                    <Row>
                        <div className="col-md-6 col-lg-3">
                            <div className="widget1">
                                <h3 className='mt-4 mb-4 text-center'> eCommerce </h3>
                                <p className='mt-2 mb-2 text-center'>SAVOLLAR BORMI? QO‘NG‘IROQ QILING!</p>
                                <h5 className='mt-2 mb-2 text-center'>+998 (71) 222-22-22</h5>
                                <div className='mt-2 mb-2 text-center'>
                                    <Button variant="primary" style={{width: '100%'}}>SHARH QOLDIRING</Button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="widget2">
                                <Nav className="flex-column">
                                    <Nav.Link href="#" className='mt-4'>Biz haqimizda</Nav.Link>
                                    <Nav.Link href="#">Shaxsiy ma’lumotlar bilan ishlash siyosati</Nav.Link>
                                    <Nav.Link href="#">Ommaviy oferta</Nav.Link>
                                    <Nav.Link href="#">Eco-friendly</Nav.Link>
                                </Nav>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="widget3">
                                <Nav className="flex-column">
                                    <Nav.Link href="#" className='mt-4'>Bo‘sh ish o‘rinlari</Nav.Link>
                                    <Nav.Link href="#">To‘lov va yetkazib berish xizmati</Nav.Link>
                                    <Nav.Link href="#">To‘lovni qaytarish va tovarlarni almashtirish</Nav.Link>
                                    <Nav.Link href="#">Bonuslar dasturi</Nav.Link>
                                </Nav>
                            </div>
                        </div>
                        <div className="col-md-6 col-lg-3">
                            <div className="widget4">
                                <Row>
                                    <p className='mt-4 mb-4'>Chegirmalar, aksiyalar va qiziqarli takliflar haqida
                                        ma‘lumot oling.</p>
                                    <Form className='mt-1 justify-content-center'>
                                        <Form.Group>
                                            <Form.Control type="email" placeholder="Enter email"/>
                                        </Form.Group>
                                        <Button variant="primary" type="submit" style={{width: '100%'}}>OBUNA
                                            BO`LISH</Button>
                                    </Form>
                                </Row>
                            </div>
                        </div>
                    </Row>
                    <hr style={{backgroundColor: "#d3d3d3"}}/>
                    <h6 className='text-center'>eCommerce © 2019. Barcha huquqlar himoyalangan</h6>
                </Container>
        )
    }

}

