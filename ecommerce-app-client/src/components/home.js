/**
 * Created by Pinup on 02.07.2019.
 */
import React from 'react';
import {connect} from 'react-redux'
import axios from 'axios'
import {Button} from "reactstrap";
import Product from './product'
import {setProducts} from '../actions/poducts'
import Layout from '../components/layout';
import {Row, Col} from 'react-bootstrap';
class Home extends React.Component {
    componentDidMount() {



        const {setBooks} = this.props;
        axios.get("/products.json")
            .then(function (response) {
                setBooks(response.data);
            })
    }

    render() {
        const products = this.props.prods.items;
        const {isReady} = this.props.prods;
        let list = [];
        if (products != null) {
            for (let i = 0; i < products.length; i++) {
                if (i <= products.length) {
                    list.push(
                        <div className='col-sm-6 col-md-6 col-lg-4 col-xl-4 mt-2'>
                            <Col><Product product={products[i]}/></Col>
                        </div>
                    );
                }
            }
        }
        return (
            <Layout>
                <h5 className='text-center mt-5'>KO‘P TANLANGAN MAHSULOTLAR</h5>
                <Row>
                    {!isReady ? 'Yuklanmoqda' : list.map(product => {
                        return (
                            product
                        )
                    })}
                </Row>
                <Row className='justify-content-center'>
                    <Button className='seeMore mt-3 mb-3'>More</Button>
                </Row>
            </Layout>
        );
    }

}

const mapStateToProps = state => ({...state});

const mapDispatchToProps = dispatch => ({
    setBooks: products => dispatch(setProducts(products))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);