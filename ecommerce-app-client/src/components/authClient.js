/**
 * Created by Pinup on 02.07.2019.
 */
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
//jojhoj
class AuthClient extends Component {
    state = {user: undefined};

    componentDidMount() {
        const token = localStorage.getItem('ecommerce-token');
        if (!token) {
            window.location.replace('/login');
        }
       else {axios({
            url: '/api/auth/user',
            headers: {Authorization: token}
        }).then(res => {
            if (res.data) {
                this.setState({user: res.data});
                console.log(res.data);
                const roles = res.data.roles;
                if (roles.filter(r => r.name === 'ROLE_ADMIN').length > 0) {
                    window.location.replace('/admin');
                } else if (roles.filter(r => r.name === 'ROLE_USER').length === 0) {
                    this.props.history.push('/login');
                }
            }
        })}
    }

    render() {
        if (this.state.user === undefined) {
            return (
                <div id="loader">
                    Please wait...
                </div>
            );
        }
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}

export default withRouter(AuthClient);