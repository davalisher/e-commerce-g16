import React from 'react'
import {Carousel} from "react-bootstrap";

export default class SlideShow extends React.Component {
    constructor() {
        super();

        this.handleSelect = this.handleSelect.bind(this);

        this.state = {
            index: 0,
            direction: null,
        };
    }

    handleSelect(selectedIndex, e) {
        this.setState({
            index: selectedIndex,
            direction: e.direction,
        });
    }

    render() {
        const { index, direction } = this.state;

        return (
            <Carousel
                activeIndex={index}
                direction={direction}
                onSelect={this.handleSelect}
                className='mt-4 mb-4'
            >
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="slideImg/slide2.png"
                        alt="Third slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="slideImg/slide3.png"
                        alt="Third slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="slideImg/slide4.png"
                        alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>
        );
    }
}
