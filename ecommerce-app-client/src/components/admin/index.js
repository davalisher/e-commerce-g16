/**
 * Created by Pinup on 04.07.2019.
 */
import React from 'react';
import {CardGroup, Container, Row, Col} from 'react-bootstrap';
import AdminHome from './adminHome';
import Product from './product';
import Category from './category';
import AuthAdmin from '../authAdmin';
import {Switch, BrowserRouter, Route, withRouter} from 'react-router-dom';

class Admin extends React.Component {
    render() {
        return (
            <Container>
                <BrowserRouter>
                    <Switch>
                        <AuthAdmin>
                            <Route path="/admin" component={AdminHome}/>
                            <Route path="/admin/product" component={Product}/>
                            <Route path="/admin/category" component={Category}/>
                        </AuthAdmin>
                    </Switch>
                </BrowserRouter>
            </Container>
        );
    }

}

export default Admin;
