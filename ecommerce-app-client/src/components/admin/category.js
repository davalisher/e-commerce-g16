import React, {Fragment} from 'react'
import Col from "react-bootstrap/Col";
import {Row} from "react-bootstrap";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Table, Spinner} from "reactstrap";
import axios from 'axios';
import {AvForm, AvField} from 'availity-reactstrap-validation';

class Category extends React.Component {
    constructor() {
        super();
        this.state = {
            list: [],
            modal: false,
            iconId: null,
            loading: false,
            modalProp:{
                catId: null,
                function: this.save
            },
            editCat:null

        }
    }

    componentDidMount() {
        this.getAllCategories();
    }

    getAllCategories = () => {
        const token = localStorage.getItem('ecommerce-token');
        axios({
            url: '/api/category/take',
            headers: {Authorization: token}
        }).then(res => {
            this.setState({list: res.data});
        })
    }
    toggle = (event) => {

        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    toggleLoading = () => {
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
    }
    save = (e, values) => {
        this.toggleLoading();
        const token = localStorage.getItem('ecommerce-token');
        values = {
            ...values,
            iconId: this.state.iconId
        }
        axios({
            url: '/api/category/save',
            method: 'post',
            data: values,
            headers: {Authorization: token}
        }).then(res => {
            this.toggleLoading();
            this.toggle();
            this.getAllCategories();
        })
    }  ;
    edit = (e, values) => {
        this.toggleLoading();
        let id=this.state.modalProp.catId;
        const token = localStorage.getItem('ecommerce-token');
        values = {
            ...values,
            iconId: this.state.iconId
        }
        axios({
            url: '/api/category/edit/'+id,
            method: 'put',
            data: values,
            headers: {Authorization: token}
        }).then(res => {
            this.state.editCat=null;
            this.toggleLoading();
            this.toggle();
            this.getAllCategories();
            console.log(res.data);

        })
    }
     setFunction=(event)=>{
        let button=event.target.name;

        if (button=="Save"){
            this.state.editCat=null
            this.state.iconId=null
            this.state.modalProp.function=this.save;
            this.setState(this.state)
            this.toggle()
        }
 if (button=="Delete"){ this.toggleLoading();
     const token = localStorage.getItem('ecommerce-token');
     let id=event.target.id
     axios({
         url: '/api/category/delete/'+id,
         method: 'delete',
         headers: {'Authorization': token},
     }).then(res => {
         this.toggleLoading();
         this.getAllCategories();
        console.log(res.data)
     })
        }
 if (button=="Edit"){
     let id=event.target.id
           this.state.modalProp.catId=id;
           this.state.editCat=this.state.list.filter(cat=>{
               return cat.id==id
           })[0];
          this.state.iconId= this.state.editCat.icon?this.state.editCat.icon.id:null
           console.log( this.state.editCat)
            this.state.modalProp.function=this.edit;
            this.setState(this.state)
            this.toggle()
        }


     }
    saveFile = (e) => {
        const formData = new FormData();
        formData.append('file', e.target.files[0]);
        //for multiple upload file
        //Array.from(e.target.files).forEach((file, i) => formData.append('files' + i, file));
        const token = localStorage.getItem('ecommerce-token');
        axios({
            url: '/api/attachment/upload',
            method: 'post',
            headers: {
                'Authorization': token,
                "Content-Type": "multipart/form-data"
            },
            data: formData,
        }).then(res => {
            this.setState({
                iconId: res.data[0]
            })
        })
    }

    render() {
        const {list, iconId, loading} = this.state;
        let parent= this.state.editCat?list.filter(cat=>{
            return cat.id==this.state.editCat.parentId})[0]:null;
        return <Fragment>
            <Button name="Save"  onClick={this.setFunction}>Add new category</Button>
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Icon</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                {list.map((item, index) => <tr>
                    <th scope="row">{index + 1}</th>
                    <td><img src={item.icon !== null ? '/api/attachment/getFile/' + item.icon.id : ''} width="100"/>
                    </td>
                    <td>{item.name} <Button name="Edit" id={item.id} onClick={this.setFunction}>Edit</Button>{loading ? <Spinner color="danger"/> :<Button name="Delete" id={item.id} variant="danger" onClick={this.setFunction}>Delete</Button>}</td>
                </tr>)}
                </tbody>
            </Table>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <AvForm onValidSubmit={this.state.modalProp.function}>
                    <ModalHeader toggle={this.toggle}>Add new category</ModalHeader>
                    <ModalBody>
                        <AvField name="name"
                                 label="Name (default error message)"
                                 type="text" errorMessage="Invalid name"
                                 validate={{required: {value: true}}}
                                 defaultValue={ this.state.editCat!=null? this.state.editCat.name:""}
                        />
                        <AvField name="parentId"
                                 label="Parent (default error message)"
                                 type="select"

                        >
                            <option value={ this.state.editCat!=null ? this.state.editCat.parentId:" "} selected>{ parent ? parent.name :" "}</option>
                            {list.map(item => <option value={item.id}>{item.name}</option>)}
                        </AvField>
                        <input type="file" name="file" onChange={this.saveFile}/>
                        <img src={'/api/attachment/getFile/' +iconId} width="100"/>
                    </ModalBody>
                    <ModalFooter>
                        {loading ? <Spinner color="primary"/> :
                            <Button color="primary">Save</Button>}{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </AvForm>
            </Modal>
        </Fragment>
    }
}

export default Category;