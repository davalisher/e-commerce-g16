import React, {Fragment} from 'react'
import {Table, Form, Col, Row, ListGroupItem} from 'react-bootstrap'

import {
    Button,
    ListGroup,
    ListGroupItemHeading,
    ListGroupItemText,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Spinner
} from "reactstrap";
import {AvForm, AvField} from 'availity-reactstrap-validation';

class Product extends React.Component {
    constructor() {
        super();
        this.state = {
            products: [{
                id: 0,
                name: 'iphone',
                price: '1000$',
                category: {
                    id: 0,
                    name: 'phone',
                    key: 0,
                    is_main: false
                },
                details: [{
                    id: 0,
                    name: 'systsaem',
                    value: 'iodasdads',
                    detailType: {
                        id: 0,
                        name: 'adasdsadll'
                    }
                }],
                attachment: [{
                    id: 0,
                    size: '0',
                    extern: 'null',
                    name: 'pic',
                    originalName: 'picNotAdded',
                    attachmentContent: {
                        id: '0',
                        file: 'dadsadda'
                    }
                }]
            }],
            categories: [{name: 'phone'}, {name: 'tv'}, {name: 'computer'}],
            details: [{
                id: 1,
                name: 'system',
                value: 'ios',
                detailType: {
                    id: 1,
                    name: 'all'
                }
            }
            ],
            setProduct: false,
            detailTypes: [{
                id: 1,
                name: 'all'
            }],
            form: null,
            editProduct: null,
            modal: {
                show: false,
                function: null,
                detail: null,
                detailType: null,
                name: null
            },
            loading: false
        }
    }

    toggleLoading = () => {
        this.setState(prevState => ({
            loading: !prevState.loading
        }));
    }
    toggle = (event) => {
        if (event) {
            let name = event.target.name;
            if (name == "EditDetailType") {
                let id = event.target.id;
                this.state.modal.detailType = this.state.detailTypes.filter(detailType => {
                    return detailType.id == id
                })[0];

                this.state.modal.function = this.addDetailTypes;
                this.state.modal.name = "EditDetailType"
            }
            if (name == "AddDetailType") {
                this.state.modal.function = this.addDetailTypes;
                this.state.modal.name = "AddDetailType"
            }
            if (name == "AddDetail") {
                let id = event.target.id;
                this.state.modal.detailType = this.state.detailTypes.filter(detailType => {
                    return detailType.id == id
                })[0];
                this.state.modal.function = this.addDetail;
                this.state.modal.name = "AddDetail"

            }
            if (name == "EditDetail") {
                let id = event.target.id;
                this.state.modal.detail = this.state.details.filter(details => {
                    return details.id == id
                })[0];
                this.state.modal.function = this.addDetailTypes;
                this.state.modal.name = "EditDetailType"
            }
            if (name == "AddNewDetailType") {
                this.state.modal.function = this.addDetailTypes;
                this.state.modal.name = "AddNewDetailType"
            }
        }
        this.state.modal.show = !this.state.modal.show
        console.log(this.state)
        this.setState(this.state);
    }
    delete = (event) => {
        let id = event.target.id;
        let buttonName = event.target.name;
        if (buttonName == "DeleteProduct") {
            this.state.products.splice(id, 1)
        }
        if (buttonName == "DetailType") {
            this.state.detailTypes.splice(id, 1)
        }
        if (buttonName == "Detail") {
            this.state.details.splice(id, 1)
        }
        console.log(this.state);
        this.setState(this.state)
    }
    setForm = (event) => {
        if (event.target.name == "CancelSave") {
            this.state.editProduct = null;
        }
        this.state.setProduct = !this.state.setProduct;
        // console.log(event.target.name)
        this.setState(this.state);
    };
    addDetail = (event, values) => {
        console.log(values)
        let detailType = this.state.modal.detailType;

        let max = 0;

        this.state.details.map(dtt => {
            if (max < dtt.id) max = dtt.id;
        });
        console.log(max);
        let detail;
        detail = {
            id: max++,
            name: values.name,
            value: values.value,
            detailType
        };

        console.log(detail);
        console.log('detail');
       this.state.details.push(detail);
        this.toggle();
        this.setState(this.state);
    };
    addDetailTypes = (event, values) => {
        if (this.state.modal.name=="EditDetailType"){

            this.state.detailTypes.splice(this.state.detailTypes.indexOf(this.state.modal.detailType),1)
            this.state.modal.detailType.name=values.name;
            this.state.detailTypes.push( this.state.modal.detailType)
        }
        else{

        let maxDetaiTypeId = this.state.detailTypes[0].id;
        this.state.detailTypes.map(dtt => {
            if (maxDetaiTypeId < dtt.id) maxDetaiTypeId = dtt.id;
        });
        let detailType = {
            id: ++maxDetaiTypeId,
            name: values.name
        };
            this.state.detailTypes.push(detailType);
            console.log(detailType)
        }

        this.toggle();

        this.setState(this.state);
    };
    editProduct = (event) => {
        let id = event.target.id;
        let product;
        this.state.products.map(product1 => {
            if (product1.id == id) product = product1;
        });
        this.state.editProduct = product;
        let detailTypes = [];
        product.details.map(detail => {
            detailTypes.push(detail.detailType)
        });
        this.state.detailTypes = detailTypes;
        this.state.details = product.details;
        this.state.setProduct = true;
        console.log(this.state)
        this.toggle();
        this.setState(this.state);
    };
    drawDetailList = (details) => {
        let {detailTypes} = this.state;
        let list = [];
        for (let i = 0; i < detailTypes.length; i++) {


            list.push(<h4>Detail Type</h4>)
            list.push(<ListGroupItem active tag="button" action>{detailTypes[i].name}</ListGroupItem>)
            list.push(<div><Button onClick={this.toggle} id={detailTypes[i].id} color="info" name="EditDetailType">Edit
                detail type</Button>
                <Button id={detailTypes[i].id} onClick={this.delete} color="danger" name="DetailType">delete detail
                    type</Button>
                <Button id={detailTypes[i].id} onClick={this.toggle} color="warning" name="AddDetailType">add detail
                    type</Button>
                <Button id={detailTypes[i].id} onClick={this.toggle} name="AddDetail">add detail</Button></div>)


            for (let j = 0; j < details.length; j++) {
                if (detailTypes[i].id == details[j].detailType.id) {
                    list.push(<h5>Detail</h5>)
                    list.push(<ListGroupItem>
                        <ListGroupItemHeading>{details[j].name}</ListGroupItemHeading>
                        <ListGroupItemText>
                            {details[j].value}
                        </ListGroupItemText>
                    </ListGroupItem>)
                    list.push(<div><Button id={this.state.detailTypes[i].id} onClick={this.toggle} color="info"
                                           name="EditDetail">edit detail</Button>
                        <Button id={this.state.detailTypes[i].id} onClick={this.toggle} color="danger" name="Detail">delete
                            detail</Button>
                    </div>)

                }
            }


            return list;
        }
    };
    workWithForm = (event, values) => {
        console.log(values)


    };
    makeTable = () => {
        return <Table striped bordered hover variant="dark">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>DetailsTypes</th>
                <th>Details</th>
                <th>Attachment</th>
            </tr>
            </thead>
            <tbody>
            {this.state.products.map(product => {
                return <tr>
                    <td>{product.id}</td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                    <td>{product.category.name}</td>
                    <td>
                        <select>
                            {product.details.map(detail => {
                                return <option id={detail.detailType.id}
                                               className={detail.id}
                                >{detail.detailType.name}</option>
                            })}
                        </select>
                    </td>
                    <td><select>
                        {product.details.map(detail => {
                            return <option id={detail.id}>{detail.name}-{detail.value}</option>
                        })} </select>
                    </td>
                    <td>{product.attachment[0].name}</td>
                    <Button id={product.id} onClick={this.editProduct}>Edit</Button>
                    <Button id={product.id} onClick={this.delete} name="DeleteProduct">Delete</Button>
                </tr>
            })}
            </tbody>
            <Button onClick={this.setForm}>add new Product</Button>
        </Table>
    };
    makeForm = () => {
        let {details} = this.state;
        let listDetails = this.drawDetailList(details);
        return <AvForm onValidSubmit={this.workWithForm}>
            <AvField name="name"
                     label="Name (default error message)"
                     type="text" errorMessage="Invalid name"
                     validate={{required: {value: true}}}
                     defaultValue={this.state.editProduct ? this.state.editProduct.name : ""}
            />
            <AvField name="price"
                     label="Price (default error message)"
                     type="text" errorMessage="Invalid name"
                     validate={{required: {value: true}}}
                     defaultValue={this.state.editProduct ? this.state.editProduct.price : ""}
            />
            <AvField name="category"
                     label="Parent (default error message)"
                     type="select"
                     validate={{required: {value: true}}}
            >
                <option
                    value={this.state.editProduct != null ? this.state.editProduct.category.id : ""}>{this.state.editProduct != null ? this.state.editProduct.category.name : ""}</option>
                {this.state.categories.map(category => {
                    return <option value={category.id}>{category.name}</option>
                })}
            </AvField>
            <ListGroup>
                {listDetails != null ? listDetails.map(detail => {
                    return detail
                }) : <div><Button onClick={this.addDetail} color="info" name="AddNewDetailType">add detail type</Button>
                </div>}
            </ListGroup>
            <input type="file" multiple/>

            <Button color="success">
                Save
            </Button>
            <Button color="danger" name="CancelSave" onClick={this.setForm}>
                Cancel
            </Button>
        </AvForm>
    }

    render() {
        console.log(this.state.form)
        let {loading} = this.state;

        const setProduct = () => {
            this.state.setProduct = true;
            this.setState(this.state);
        }

        return <Fragment>{!this.state.setProduct ? this.makeTable() : this.makeForm()}
            <Modal isOpen={this.state.modal.show} toggle={this.toggle}>
                <AvForm onValidSubmit={this.state.modal.function}>
                    <ModalHeader toggle={this.toggle}>{this.state.modal.name}</ModalHeader>
                    <ModalBody>
                        <AvField name="name"
                                 label="Name (default error message)"
                                 type="text" errorMessage="Invalid name"
                                 validate={{required: {value: true}}}
                                 defaultValue={this.state.modal.detail != null ? this.state.modal.detail.name : this.state.modal.detailType != null ? this.state.modal.detailType.name : ""}
                        />
                        {(this.state.modal.name == "AddDetail" || this.state.modal.name == "EditDetail") ?
                            <AvField name="value"
                                     label="Value (default error message)"
                                     type="text" errorMessage="Invalid name"
                                     validate={{required: {value: true}}}
                                     defaultValue={this.state.modal.detail != null ? this.state.modal.detail.value : ""}
                            /> : ""}

                    </ModalBody>
                    <ModalFooter>
                        {loading ? <Spinner color="primary"/> :
                            <Button color="primary" name={this.state.modal.name}>Save</Button>}{' '}
                        <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </AvForm>
            </Modal>

        </Fragment>


    }
}

export default Product;