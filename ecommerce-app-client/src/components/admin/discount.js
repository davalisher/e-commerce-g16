import React, {Fragment} from 'react'
import {InputGroup, FormControl, Button, Row, ListGroupItem} from 'react-bootstrap'
// 28.06.2019
class Discount extends React.Component{
    constructor(){
        super();
        this.state = {
            products: [{
                id: 0,
                name: 'iphone',
                price: '1000$',
                categoria: {
                    id: 0,
                    name: 'phone',
                    key: 0,
                    is_main: false
                },
                details: [{
                    id: 0,
                    name: 'system',
                    value: 'ios',
                    detailTypes: {
                        id: 0,
                        name: 'all'
                    }
                }],}],
            discount:[{
                id:1,
                percentage:0,
                startDate:'2019-01-01',
                expireDate:'2019-01-01'
            },{id:2,
                percentage:0,
                startDate:'2019-01-01',
                expireDate:'2019-01-01'
            },{id:3,
                percentage:0,
                startDate:'2019-01-01',
                expireDate:'2019-01-01'
            },{id:4,
                percentage:0,
                startDate:'2019-01-01',
                expireDate:'2019-01-01'
            }],
            changedDate:false
}}
addNewDiscount=()=>{
        let max=this.state.discount[0].id;
        this.state.discount.map(dis=>{
            if (max<dis.id)max=dis.id;
        })
        let discount={
            id:++max,
            startDate:null,
            expireDate:null
        }
        this.state.discount.push(discount);
        this.setState(this.state);

}
drawDiscount=()=>{

        let list=[];
        const onchanged=(id)=>{
            return <Button id={id} onClick={this.getDate}>save</Button>
        }
        const onchangeddate=()=>{this.state.changedDate=true;
            this.setState(this.state)
        }

  this.state.discount.map(dis=>{
    let startDate=  dis.startDate;
   
    let expireDate=  dis.expireDate;
   
    let className=dis.id;
      list.push(<div><InputGroup>
          <FormControl id={dis.id}
              placeholder="presentage"
              aria-label="Recipient's username"
              aria-describedby="basic-addon2"
              className={"presentage"+className}
                       onChange={onchangeddate}
          />
          <input id={dis.id} type="date" className={"startDate"+className} defaultValue={startDate} onChange={onchangeddate}/>
          <input id={dis.id} type="date" className={"expireDate"+className} defaultValue={expireDate}onChange={onchangeddate}/>
          <InputGroup.Append>
              <Button variant="outline-secondary" onClick={this.addNewDiscount} >Button</Button>
              <Button variant="outline-secondary" id={dis.id}  >Button</Button>
              {this.state.changedDate?onchanged(dis.id):""}
          </InputGroup.Append>
      </InputGroup></div>)
  })
return list;

}
getDate=(event)=>{let id=event.target.id;
         let startDate= event.target.parentNode.parentNode.querySelector(".startDate"+id).value;
         let expireDate= event.target.parentNode.parentNode.querySelector(".expireDate"+id).value;
         let precentagge= event.target.parentNode.parentNode.querySelector(".presentage"+id).value;
         let discount={
             precentagge,
             startDate,
             expireDate
         }
          console.log(discount)
    this.setState(this.state)
}
render() {
        return <Fragment>{this.drawDiscount().map(list=>{return list})}</Fragment>
    }   }
export default Discount;